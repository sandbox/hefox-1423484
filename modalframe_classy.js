
(function ($) {

Drupal.behaviors.modalframeClassy = function(context) {
  $('.modalframe-classy-modalframe:not(.modalframe-classy-processed)', context).addClass('modalframe-classy-processed').click(function() {
    var element = this;

    // This is our onSubmit callback that will be called from the child window
    // when it is requested by a call to modalframe_close_dialog() performed
    // from server-side submit handlers.
    function onSubmitCallbackModalframeClassy(args, statusMessages) {
      if (args.redirect) {
        setTimeout(function () { window.location = args.redirect;}, 1);
      }
      else {
        location.reload();
      }
    }
    var url = $(element).attr('href');
    if (url && url.indexOf("?") != -1) {
      url = url + "&";
    }
    else {
      url = url + "?"
    }
    url = url + "modalframe-classy=TRUE";

    // Build modal frame options.
    var modalOptions = {
      url: url,
      onSubmit: onSubmitCallbackModalframeClassy
    };

    // Open the modal frame dialog.
    Drupal.modalFrame.open(modalOptions);

    // Prevent default action of the link click event.
    return false;
  });
};
})(jQuery);
